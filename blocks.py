import registry
import pygame

class Block(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.eaten = False

    def draw(self):
        pygame.draw.rect(registry.screen, registry.block_color, (self.pixel_x, self.pixel_y, registry.block_width, registry.block_height))

    def update(self):
        self.pixel_x = registry.block_offset_x + self.x * registry.block_width
        self.pixel_y = registry.block_offset_y + self.y * registry.block_height

        if self.x > 0:
            self.pixel_x = self.pixel_x + registry.block_margin_x*self.x

        if self.y > 0:
            self.pixel_y = self.pixel_y + registry.block_margin_y*self.y

        self.draw()

class Blocks(list):
    def __init__(self, num_x, num_y):
        self.num_x = num_x
        self.num_y = num_y

        self.init_blocks()
        #super?

    def init_blocks(self):
        for x_block in range(self.num_x):
            for y_block in range(self.num_y):
                self.append(Block(x_block, y_block))

    def update(self):
        for block in self:
            if block.eaten == False:
                block.update()

    def events(self, event):
        pass
