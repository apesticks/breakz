import os, pygame
from player import Player
from border import Border
from blocks import Blocks
from ball import Ball

#sets the screen to appear in same position every time
window_position = 790, 250
os.environ["SDL_VIDEO_WINDOW_POS"] = str(window_position[0]) + "," + str(window_position[1])

screen_size=(800,600)
screen = pygame.display.set_mode(screen_size, 0, 32)

block_color = (255, 255, 255)
block_width = 75
block_height = 20
block_offset_x = 20
block_offset_y = 20
block_margin_x = 10
block_margin_y = 10

move_speed = 8
started = False

player = Player()
ball = Ball()
blocks = Blocks(9, 6)

modules = [
    player,
    ball,
    blocks,
    Border(),
]

