import pygame, pdb
from pygame.locals import *
import registry

pygame.init()
clock = pygame.time.Clock()
running = True

def run():
    while running:
        registry.screen.fill((0,0,0))
        clock.tick(60)
        events()
        update()
        pygame.display.update()

def events():
    for event in pygame.event.get():
        game_quit_events(event)
        for module in registry.modules:
            if hasattr(module, "events"):
                module.events(event)

def game_quit_events(event):
    if event.type == QUIT:
        running = False
    if event.type == KEYDOWN:
       if event.key == K_ESCAPE:
           running = False

def update():
    for module in registry.modules:
        module.update()

if __name__ == '__main__':
    run()
