import registry
import pygame

class Border(object):
    def lines(self):
        x_max = registry.screen_size[0] - 1
        y_max = registry.screen_size[1] - 1

        return [
            (0, 0),
            (0, y_max),
            (x_max, y_max),
            (x_max, 0),
            (0, 0)
        ]

    def update(self):
        pygame.draw.lines(registry.screen, (255, 0, 255), True, self.lines(), 1)

    def events(self, event):
        pass
