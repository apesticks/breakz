import pygame, pdb
import registry
from pygame import *

class Player(object):
    def __init__(self):
        self.width = registry.block_width * 2
        self.height = 20

        self.move_right = False
        self.move_left = False

        self.x = (registry.screen_size[0] / 2) - (self.width / 2)
        self.y = (registry.screen_size[1]) - self.height - 5

    def draw(self):
        pygame.draw.rect(registry.screen, (255, 255, 255), (self.x, self.y, self.width, self.height))

    def move(self):
        if self.move_right == True:
            self.x = self.x + registry.move_speed

        if self.move_left == True:
            self.x = self.x - registry.move_speed

    def update(self):
        pygame.display.set_caption("{}".format((self.x, self.y, self.move_right, self.move_left)))

        self.move()
        self.draw()

    def events(self, event):
        if event.type == KEYDOWN:
            registry.started = True

            if event.key == K_RIGHT:
                self.move_right = True

            if event.key == K_LEFT:
                self.move_left = True

        if event.type == KEYUP:
            if event.key == K_RIGHT:
                self.move_right = False

            if event.key == K_LEFT:
                self.move_left = False
