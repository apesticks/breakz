import pygame, random
import registry

class Ball(object):
    def __init__(self):
        self.size = 8
        self.speed = 4

        self.die()

    def wall_collision(self):
        if self.x < 0:
            self.direction_x = 1

        if self.x > registry.screen_size[0]:
            self.direction_x = -1

        if self.y < 0:
            self.direction_y = 1

        if self.y > registry.screen_size[1]:
            self.die()

    def paddle_collision(self):
        player = registry.player

        if self.x in range(player.x, player.x + player.width):
            if self.y in range(player.y, player.y + player.height):
                self.direction_y = -1

    def block_collision(self):
        if registry.started == True:
            for block in registry.blocks:
                if self.x in range(block.pixel_x, block.pixel_x + registry.block_width):
                    if self.y in range(block.pixel_y, block.pixel_y + registry.block_height):
                        if block.eaten == False:
                            block.eaten = True
                            self.direction_y = 1
                            #self.direction_x = not self.direction_x

    def collision(self):
        self.wall_collision()
        self.paddle_collision()
        self.block_collision()

    def die(self):
        registry.started = False

        self.direction_x = random.choice([-1, 1])
        self.direction_y = -1

        self.x = (registry.screen_size[0] / 2) - (self.size / 2)
        self.y = registry.screen_size[1] - registry.block_height - self.size*2

    def draw(self):
        pygame.draw.circle(registry.screen, (255, 0, 0), (self.x, self.y), self.size)

    def move(self):
        if registry.started == True:
            self.x = self.x + self.speed*self.direction_x
            self.y = self.y + self.speed*self.direction_y

        self.collision()

    def update(self):
        self.draw()
        self.move()

    def events(self, event):
        pass
